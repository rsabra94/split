#include <iostream>
#include <fstream>
#include <sstream>
#include <exception>
#include <math.h>

#include <tclap/CmdLine.h>

inline std::ifstream open_input_chunk_file(std::string& input_chunk_filepath_template, size_t chunk_idx, std::string& input_chunk_filepath)
{
    std::stringstream input_chunk_filepath_ss;
    input_chunk_filepath_ss << input_chunk_filepath_template << "." << chunk_idx << ".part";
    input_chunk_filepath = input_chunk_filepath_ss.str();
    std::ifstream input_chunk_file {input_chunk_filepath, std::ifstream::ate | std::ifstream::binary};
    if (!input_chunk_file.is_open())
    {
        std::stringstream error_ss;
        error_ss << "Chunk file \"" << input_chunk_filepath << "\" is missing.";
        throw std::runtime_error(error_ss.str());
    }
    return input_chunk_file;
}

inline bool input_chunk_file_size_as_expected(std::string& input_chunk_filepath, std::ifstream& input_chunk_file, size_t expected_chunk_size, size_t& input_chunk_file_size)
{
    input_chunk_file_size = input_chunk_file.tellg();
    return input_chunk_file_size == expected_chunk_size;
}

inline void verify_input_chunk_file_size(std::string& input_chunk_filepath, std::ifstream& input_chunk_file, size_t expected_chunk_size)
{
    size_t input_chunk_file_size = input_chunk_file.tellg();
    if (input_chunk_file_size != expected_chunk_size)
    {
        std::stringstream error_ss;
        error_ss << "Chunk file \"" << input_chunk_filepath << "\" size (" << input_chunk_file_size << ") does not match expected size (" << expected_chunk_size << ").";
        throw std::runtime_error(error_ss.str());
    }
}

inline void verify_previous_chunk_file_size_as_expected(std::string& input_chunk_filepath_template, size_t prev_chunk_size_as_expected, size_t prev_chunk_idx, size_t prev_chunk_size, size_t expected_chunk_size)
{
    if (!prev_chunk_size_as_expected)
    {
        std::stringstream input_chunk_filepath_ss;
        input_chunk_filepath_ss << input_chunk_filepath_template << "." << prev_chunk_idx << ".part";
        std::string input_chunk_filepath = input_chunk_filepath_ss.str();
        std::stringstream error_ss;
        error_ss << "Chunk file \"" << input_chunk_filepath << "\" size (" << prev_chunk_size << ") does not match expected size (" << expected_chunk_size << ").";
    }
}

inline void write_chunk_data_to_file(std::ifstream& input_chunk_file, std::ostream& output_file, char* buf, size_t chunk_size)
{
    input_chunk_file.seekg(std::ifstream::beg);
    input_chunk_file.read(buf, chunk_size);
    output_file.write(buf, chunk_size);
}

inline void write_last_chunk_to_file(std::string& input_chunk_filepath_template, size_t expected_num_chunks, std::ofstream& output_file, char* buf)
{
    std::string last_input_chunk_filepath;
    std::ifstream last_input_chunk_file = open_input_chunk_file(input_chunk_filepath_template, expected_num_chunks - 1, last_input_chunk_filepath);
    size_t last_input_chunk_size = last_input_chunk_file.tellg();
    write_chunk_data_to_file(last_input_chunk_file, output_file, buf, last_input_chunk_size);
}

int main(int argc, char* argv[])
{
    std::string input_chunk_filepath_template, output_filepath;
    bool expected_chunk_size_is_set, expected_num_chunks_is_set;
    size_t expected_chunk_size, expected_num_chunks;

    try
    {
        TCLAP::CmdLine cmd("Flexible file chunk joiner");
        TCLAP::UnlabeledValueArg<std::string> input_chunk_filepath_template_arg("input_chunk_filepath_template", "input chunk filepath template", true, "", "inputChunkFilepathTemplate", cmd);
        TCLAP::UnlabeledValueArg<std::string> output_filepath_arg("output_filepath", "output filepath", true, "", "outputFilepath", cmd);
        TCLAP::ValueArg<std::string> expected_chunk_size_arg("s", "chunksize", "expected chunk size", false, "", "expectedChunkSize", cmd);
        TCLAP::ValueArg<size_t> expected_num_chunks_arg("n", "numchunks", "expected number of chunks", false, 0, "expectedNumChunks", cmd);
        cmd.parse(argc, argv);

        input_chunk_filepath_template = input_chunk_filepath_template_arg.getValue();
        output_filepath = output_filepath_arg.getValue();

        expected_chunk_size_is_set = expected_chunk_size_arg.isSet();
        expected_num_chunks_is_set = expected_num_chunks_arg.isSet();

        if (expected_chunk_size_is_set)
        {
            std::string expected_chunk_size_str = expected_chunk_size_arg.getValue();
            char suffix = expected_chunk_size_str[expected_chunk_size_str.size() - 1];
            size_t multiplier;
            switch (suffix)
            {
                case 'K':
                    multiplier = 1UL << 10;
                    break;
                case 'M':
                    multiplier = 1UL << 20;
                    break;
                case 'G':
                    multiplier = 1UL << 30;
                    break;
                case 'T':
                    multiplier = 1UL << 40;
                    break;
                case 'P':
                    multiplier = 1UL << 50;
                    break;
                case 'E':
                    multiplier = 1UL << 60;
                    break;
                case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
                    multiplier = 1;
                    break;
                default:
                    std::cerr << "Invalid suffix: \'" << suffix << "\'\n";
                    return 1;
            }
            std::stringstream expected_chunk_size_ss;
            expected_chunk_size_ss << expected_chunk_size_str.substr(0, expected_chunk_size_str.size() - (multiplier != 1));
            expected_chunk_size_ss >> expected_chunk_size;
            expected_chunk_size *= multiplier;
        }
        if (expected_num_chunks_is_set) expected_num_chunks = expected_num_chunks_arg.getValue();
    }
    catch(const TCLAP::ArgException& e)
    {
        std::cerr << e.error() << " for arg " << e.argId() << "\n";
        return 1;
    }

    std::ofstream output_file {output_filepath};
    char *buf;
    if (expected_chunk_size_is_set)
    {
        buf = new char[expected_chunk_size];
        if (expected_num_chunks_is_set)
        {
            for (size_t chunk_idx = 0; chunk_idx < expected_num_chunks - 1; ++chunk_idx)
            {
                std::string input_chunk_filepath;
                std::ifstream input_chunk_file = open_input_chunk_file(input_chunk_filepath_template, chunk_idx, input_chunk_filepath);
                verify_input_chunk_file_size(input_chunk_filepath, input_chunk_file, expected_chunk_size);
                write_chunk_data_to_file(input_chunk_file, output_file, buf, expected_chunk_size);
            }

            write_last_chunk_to_file(input_chunk_filepath_template, expected_num_chunks, output_file, buf);
        }
        else
        {
            size_t chunk_idx = 0;
            bool chunk_size_as_expected = true;
            size_t chunk_size;
            while (true)
            {
                std::string input_chunk_filepath;
                std::ifstream input_chunk_file;
                try { input_chunk_file = open_input_chunk_file(input_chunk_filepath_template, chunk_idx, input_chunk_filepath); }
                catch(const std::runtime_error& e) { break; }
                verify_previous_chunk_file_size_as_expected(input_chunk_filepath_template, chunk_size_as_expected, chunk_idx - 1, chunk_size, expected_chunk_size);
                chunk_size_as_expected = input_chunk_file_size_as_expected(input_chunk_filepath, input_chunk_file, expected_chunk_size, chunk_size);
                write_chunk_data_to_file(input_chunk_file, output_file, buf, chunk_size);
                ++chunk_idx;
            }
        }
    }
    else
    {
        std::string input_chunk_filepath;
        std::ifstream first_input_chunk_file = open_input_chunk_file(input_chunk_filepath_template, 0, input_chunk_filepath);
        size_t first_chunk_size = first_input_chunk_file.tellg();
        buf = new char[first_chunk_size];
        write_chunk_data_to_file(first_input_chunk_file, output_file, buf, first_chunk_size);
        size_t chunk_idx = 1;
        
        if (expected_num_chunks_is_set)
        {
            for (; chunk_idx < expected_num_chunks - 1; ++chunk_idx)
            {
                std::string input_chunk_filepath;
                std::ifstream input_chunk_file = open_input_chunk_file(input_chunk_filepath_template, chunk_idx, input_chunk_filepath);
                size_t chunk_size = input_chunk_file.tellg();
                verify_input_chunk_file_size(input_chunk_filepath, input_chunk_file, first_chunk_size);
                write_chunk_data_to_file(input_chunk_file, output_file, buf, first_chunk_size);
            }

            write_last_chunk_to_file(input_chunk_filepath_template, expected_num_chunks, output_file, buf);
        }
        else
        {
            bool chunk_size_as_expected = true;
            size_t chunk_size;
            while (true)
            {
                std::string input_chunk_filepath;
                std::ifstream input_chunk_file;
                try { input_chunk_file = open_input_chunk_file(input_chunk_filepath_template, chunk_idx, input_chunk_filepath); }
                catch(const std::runtime_error& e) { break; }
                verify_previous_chunk_file_size_as_expected(input_chunk_filepath_template, chunk_size_as_expected, chunk_idx - 1, chunk_size, first_chunk_size);
                chunk_size_as_expected = input_chunk_file_size_as_expected(input_chunk_filepath, input_chunk_file, first_chunk_size, chunk_size);
                write_chunk_data_to_file(input_chunk_file, output_file, buf, chunk_size);
                ++chunk_idx;
            }
        }
        
    }
    
}