#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

#include <tclap/CmdLine.h>

inline void write_data_to_chunk(std::ifstream& input_file, std::string& output_chunk_filepath_template, size_t chunk_idx, char* buf, size_t chunk_size, size_t buf_size)
{
    std::stringstream output_chunk_filepath_ss;
    output_chunk_filepath_ss << output_chunk_filepath_template << "." << chunk_idx << ".part";
    std::string output_chunk_filepath = output_chunk_filepath_ss.str();
    std::ofstream output_chunk_file {output_chunk_filepath, std::ofstream::binary};
    for (size_t i = 0; i < chunk_size / buf_size; ++i)
    {
        input_file.read(buf, buf_size);
        output_chunk_file.write(buf, buf_size);
    }
    size_t remaining_size = chunk_size % buf_size;
    input_file.read(buf, remaining_size);
    output_chunk_file.write(buf, remaining_size);
}

inline size_t get_multiplier(char suffix)
{
    size_t multiplier;
    switch (suffix)
    {
        case 'K':
            multiplier = 1UL << 10;
            break;
        case 'M':
            multiplier = 1UL << 20;
            break;
        case 'G':
            multiplier = 1UL << 30;
            break;
        case 'T':
            multiplier = 1UL << 40;
            break;
        case 'P':
            multiplier = 1UL << 50;
            break;
        case 'E':
            multiplier = 1UL << 60;
            break;
        case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
            multiplier = 1;
            break;
        default:
            std::stringstream error_ss;
            error_ss << "Invalid suffix: \'" << suffix << "\'\n";
            throw std::logic_error(error_ss.str());
    }
    return multiplier;
}

constexpr size_t DEFAULT_BUF_SIZE = 33554432;

int main(int argc, char* argv[])
{
    std::string input_filepath;
    size_t chunk_size;
    std::string output_chunk_filepath_template;
    bool chunk_start_idx_is_set, chunk_end_idx_is_set;
    size_t chunk_start_idx, chunk_end_idx;
    size_t buf_size;

    try
    {
        TCLAP::CmdLine cmd("Flexible file splitter");
        TCLAP::UnlabeledValueArg<std::string> input_filepath_arg("input_filepath", "input filepath", true, "", "inputFilepath", cmd);
        TCLAP::UnlabeledValueArg<std::string> chunk_size_arg("chunksize", "chunk size", true, "", "chunksize", cmd);
        TCLAP::UnlabeledValueArg<std::string> output_chunk_filepath_template_arg("output_chunk_filepath_template", "output chunk filepath template", true, "", "outputChunkFilepathTemplate", cmd);
        TCLAP::ValueArg<size_t> chunk_start_idx_arg("s", "start", "chunk start index", false, 0, "chunkStartIndex", cmd);
        TCLAP::ValueArg<size_t> chunk_end_idx_arg("e", "end", "chunk end index", false, 0, "chunkEndIndex", cmd);
        TCLAP::ValueArg<std::string> buf_size_arg("b", "buffer_size", "buffer size", false, "", "bufferSize", cmd);
        cmd.parse(argc, argv);

        input_filepath = input_filepath_arg.getValue();
        
        std::string chunk_size_str = chunk_size_arg.getValue();
        char chunk_size_suffix = chunk_size_str[chunk_size_str.size() - 1];
        size_t chunk_size_multiplier = get_multiplier(chunk_size_suffix);
        std::stringstream chunk_size_ss;
        chunk_size_ss << chunk_size_str.substr(0, chunk_size_str.size() - (chunk_size_multiplier != 1));
        chunk_size_ss >> chunk_size;
        chunk_size *= chunk_size_multiplier;
        
        if (buf_size_arg.isSet())
        {
            std::string buf_size_str = buf_size_arg.getValue();
            char buf_size_suffix = buf_size_str[buf_size_str.size() - 1];
            size_t buf_size_multiplier = get_multiplier(buf_size_suffix);
            std::stringstream buf_size_ss;
            buf_size_ss << buf_size_str.substr(0, buf_size_str.size() - (buf_size_multiplier != 1));
            buf_size_ss >> buf_size;
            buf_size *= buf_size_multiplier;
        }
        else { buf_size = DEFAULT_BUF_SIZE; }

        output_chunk_filepath_template = output_chunk_filepath_template_arg.getValue();
        chunk_start_idx_is_set = chunk_start_idx_arg.isSet();
        chunk_end_idx_is_set = chunk_end_idx_arg.isSet();
        if (chunk_start_idx_is_set) chunk_start_idx = chunk_start_idx_arg.getValue();
        if (chunk_end_idx_is_set) chunk_end_idx = chunk_end_idx_arg.getValue();
    }
    catch(const TCLAP::ArgException& e)
    {
        std::cerr << e.error() << " for arg " << e.argId() << "\n";
        return 1;
    }

    std::ifstream input_file {input_filepath, std::ifstream::ate | std::ifstream::binary};
    size_t input_file_size = input_file.tellg();
    size_t num_full_chunks = input_file_size / chunk_size;
    size_t last_chunk_size = input_file_size % chunk_size;
    size_t num_chunks = num_full_chunks + (last_chunk_size != 0);

    if (!chunk_start_idx_is_set) { chunk_start_idx = 0; }
    else if(chunk_start_idx >= num_chunks) { std::cerr << "Invalid chunk start index: " << chunk_start_idx << "\n"; return 1; }
    
    if (!chunk_end_idx_is_set) { chunk_end_idx = num_chunks - 1; }
    else if(chunk_end_idx >= num_chunks) { std::cerr << "Invalid chunk end index: " << chunk_end_idx << "\n"; return 1; }
    
    if(chunk_start_idx_is_set && chunk_end_idx_is_set && chunk_end_idx < chunk_start_idx) { std::cerr << "Chunk end index (" << chunk_end_idx << ") is less than chunk start index (" << chunk_start_idx << ")\n"; return 1; }
    
    size_t start_offset = chunk_start_idx * chunk_size;
    input_file.seekg(start_offset);
    char *buf = new char[buf_size];
    for (size_t chunk_idx = chunk_start_idx; chunk_idx < chunk_end_idx; ++chunk_idx) { write_data_to_chunk(input_file, output_chunk_filepath_template, chunk_idx, buf, chunk_size, buf_size); }
    if (last_chunk_size > 0 && chunk_end_idx == num_full_chunks) { write_data_to_chunk(input_file, output_chunk_filepath_template, num_full_chunks, buf, last_chunk_size, buf_size); }
    else write_data_to_chunk(input_file, output_chunk_filepath_template, chunk_end_idx, buf, chunk_size, buf_size);
}